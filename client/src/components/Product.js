import React, { Component } from "react";

export default class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedProduct: {},
            isAdded: false
        }
    }

    addToCart(image, name, price, id, quantity) {
        this.setState({
            selectedProduct: {
                image: image,
                name: name,
                price: price,
                id: id,
                quantity: quantity
            }
        }, () => this.props.addToCart(this.state.selectedProduct))

        this.setState({ 
            isAdded: true
        }, function() {
            setTimeout(() => {
                this.setState({ isAdded: false, selectedProduct: {} })
            },2000)
        })
    }

    render() {
        let image = this.props.image;
        let name = this.props.name;
        let price = this.props.price;
        let id = this.props.id;
        let quantity = this.props.quantity;
        
        return(
            <div className="col mb-4">
                <div className="card product">
                        <div className="card-body p-0">
                            <div className="product__image">
                                <img 
                                    className="card-img-top"
                                    alt={name}
                                    src={image}
                                    />
                                </div>
                                <div className="pr-4 pl-4 pb-4">
                                    <h5 className="product__name">{name}</h5>
                                    <p className="product-price">$ {price}</p>
                                <button 
                                    type="button" 
                                    className={!this.state.isAdded ? "btn btn-outline-info" : "btn btn__product-added" }
                                    onClick={this.addToCart.bind(this, image, name, price, id, quantity)}
                                    >{!this.state.isAdded ? 'Add to cart' : 'Product added'}</button>
                                </div>
                        </div>
                    </div>
            </div>
        )
    }
}