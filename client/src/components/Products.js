import React, { Component } from "react";
import Product from "./Product";
import Slider from 'react-slick';

export default class Products extends Component {
    constructor() {
        super();
    }
    render() {
        const settings = {
            arrows: false,
            dots: false,
            infinite: true,
            speed: 600,
            slidesToShow: 3,
            slidesToScroll: 2,
            autoplay: true,
            autoplaySpeed: 2800
          };

        let productsData = this.props.productsList.map(product => {
            return <Product 
                        key={product.id}
                        price={product.price}
                        name={product.name}
                        image={product.image}
                        id={product.id}
                        addToCart={this.props.addToCart}
                    />
        })

        return (
                <Slider {...settings}>
                    {productsData} 
                </Slider>
        )
    }
}