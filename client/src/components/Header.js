import React, { Component } from 'react';

export default class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showCart: false,
            cart: null
        }
    }

    render() {
        let itemsInCart = this.props.cartItems.map(item => {
            return  <tr key={item.id}>
                        <th scope="row"><img className="img-fluid" src={item.image} /></th>
                        <td>{item.name}</td>
                        <td>{item.price}</td>
                        <td>X</td>                        
                    </tr>
        })
        return(
            <div className="jumbotron jumbotron-fluid">
                <div className="container">
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <a className="navbar-brand" href="#"><ion-icon name="watch"></ion-icon>WatchMe</a>

                        <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
                            <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                                <li className="nav-item active">
                                    <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Products</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Contact us</a>
                                </li>
                                </ul>
                                <form className="search-form form-inline">
                                    <ion-icon name="search"></ion-icon>
                                    <input className="form-control" type="search" placeholder="Type for search" aria-label="Search" />
                                </form>
                        </div>
                        <div className="cart pl-3 text-dark">
                            <div className="dropdown">
                                <button className="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                   <span className="badge badge-pill badge-danger">{ this.props.totalItems }</span>
                                    <ion-icon name="cart"></ion-icon>
                                </button>
                                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <table className="table">
                                            <tboby>
                                                { this.props.cartItems.length == 0 ? <tr><td>The cart is empty</td></tr> :  itemsInCart  }
                                            </tboby>
                                    </table>
                                </div>
                            </div>
                        </div>
                </nav>

                    <h1 className="display-4">Smart watches</h1>
                    <p className="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
                
                    <a href="#" className="jumbo-btn btn btn-primary">New collection</a>
                </div>
            </div>
        )
    }
}