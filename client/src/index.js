import React , { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Header from './components/Header';
import Products from './components/Products';
import Banner from './components/Banner';
import Brands from './components/Brands';
import Footer from './components/Footer';

import API from './API' 

class App extends Component {
    constructor() {
        super();
        this.state = {
            products: [],
            cart: [],
            totalItems: 0,
            totalAmount: 0
        }

        this.handleAddToCart = this.handleAddToCart.bind(this);
    }

    getProducts() {
        const url = 'products.json';

        // axios.get(url).then(response => {
        //     console.log(response);
            this.setState({
                products: API()
            })
        // })
    }

    componentWillMount() {
        this.getProducts()
    }

    handleAddToCart(product) {
        let cart = this.state.cart;
        let id = product.id;
        if (this.checkProduct(id)) {
            let index = cart.findIndex(x => x.id == id)
            cart[index].quantity++
            this.setState({ cart: cart })
        } else {
            cart.push(product)
        }
        this.setState({ cart: cart })
        this.sumTotalItems(this.state.cart)
        this.sumTotalAmount(this.state.cart)
    }

    sumTotalItems() {
        this.setState({ 
            totalItems: this.state.cart.length 
        })
    }

    sumTotalAmount() {
        let total = 0;
        let cart = this.state.cart;

        for (var i = 0; i < cart.length; i++) {
            let item = cart[i]
            total += item.price * parseInt(item.quantity)
        }
        this.setState({
            totalAmount: total
        })
    }

    checkProduct(id) {
        return this.state.cart.some((item) => {
            return item.id === id
        })
    }

    render() {
        if(this.state.products.length == 0) return <div>Loading...</div>
        
        return(
            <div className="container-fluid p-0">
                <Header 
                    cartItems={this.state.cart} 
                    totalItems={this.state.totalItems}
                    />
                    <div className="container">
                            <h2 className="section-heading">New Arrivals</h2>
                        <Products 
                            addToCart={this.handleAddToCart}
                            productsList={this.state.products} />
                    </div>
                    <Banner />
                    <div className="container">
                            <h2 className="section-heading">Popular Products</h2>
                        <Products 
                            addToCart={this.handleAddToCart}
                            productsList={this.state.products} />
                    </div>
                    <Brands />
                    <Footer />
            </div>
        )
    }
}

ReactDOM.render(<App />, document.querySelector('#root'));