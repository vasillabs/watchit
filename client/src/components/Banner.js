import React from 'react';

const Banner = () => {
    return(
        <div className="container">
            <div className="banner">
                <div className="text-box">
                    <h4 className="text-box__discount">-60%</h4>
                    <h3 className="text-box__heading">Global Sale</h3>
                    <a href="" className="text-box__btn">Buy now</a>
                </div>
            </div>
        </div>
    )
}

export default Banner;